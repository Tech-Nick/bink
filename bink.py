from MastData import MastData


if __name__ == "__main__":

    mast_data = MastData('Python Developer Test Dataset.csv')

    opt = {'2': mast_data.lease_startdate_between,
           '3': mast_data.mast_count,
           '4': mast_data.lease_years_25,
           '5': mast_data.sum_rent,
           '6': mast_data.sort_by_rent,
           '7': mast_data.first_five}
    run = True
    while run:
        print("")
        print("0 Exit program")
        print("1 Run all tasks")
        for k, v in opt.items():
            print(k, "Run " + v.__name__)
        print("")
        val = input("Please select a task to run : \n")
        if val == '0':
            run = False
        elif val == '1':
            print("Displaying all tasks results : \n")
            mast_data.printd_str(opt['3'](mast_data.csv_data), opt, '3')
            mast_data.printl_str(opt['2'](mast_data.csv_data), opt, '2')
            mast_data.printl_str(opt['4'](mast_data.csv_data), opt, '4')
            mast_data.printl_str(opt['6'](mast_data.csv_data), opt, '6')
            mast_data.printl_str(opt['7'](mast_data.csv_data), opt, '7')
            print("Displaying {opt['5'].__name__} results : \n")
            print(mast_data.sum_rent(mast_data.csv_data))
            print("")
        elif val in opt and val == '3':
            mast_data.printd_str(opt[val](mast_data.csv_data), opt, val)
        elif val in opt and val in ['2', '4', '6', '7']:
            mast_data.printl_str(opt[val](mast_data.csv_data), opt, val)
        elif val in opt and val == '5':
            print(f"Displaying {opt[val].__name__} results : \n")
            print(mast_data.sum_rent(mast_data.csv_data))
            print("")
        else:
            print("invalid input, please enter a number from 0 to 7")
            print("")
