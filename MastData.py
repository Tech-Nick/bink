import csv
from datetime import datetime


class MastData():

    def __init__(self, file):
        self.file = file
        try:
            with open(self.file) as csvfile:

                self.reader = csv.reader(csvfile)

                self.header = next(self.reader)
                self.csv_data = []

                for row in self.reader:
                    propertyName = row[0]
                    propertyAddress1 = row[1]
                    propertyAddress2 = row[2]
                    propertyAddress3 = row[3]
                    propertyAddress4 = row[4]
                    unitName = row[5]
                    tenantName = row[6]
                    leaseStart = datetime.strptime(row[7], '%d %b %Y').date()
                    leaseEnd = datetime.strptime(row[8], '%d %b %Y').date()
                    leaseYears = int(row[9])
                    currentRent = float(row[10])

                    self.csv_data.append([propertyName,
                                          propertyAddress1,
                                          propertyAddress2,
                                          propertyAddress3,
                                          propertyAddress4,
                                          unitName,
                                          tenantName,
                                          leaseStart,
                                          leaseEnd,
                                          leaseYears,
                                          currentRent])
        except OSError as err:
            print("OS error: {0}".format(err))
            raise
        except ValueError as err:
            print("Could not convert data to date. {0}".format(err))
            raise
        except Exception as err:
            print("Unexpected error: {0}".format(err))
            raise

    #  sorted by current rent

    def sort_by_rent(self, data):
        self.data = data
        sortedData = sorted(self.data, key=lambda x: x[10])
        return sortedData

    # the first 5 entry's sorted by current rent

    def first_five(self, data):
        self.data = data
        sortedData = sorted(self.data, key=lambda x: x[10])
        return sortedData[0:5]

    # lease years = 25

    def lease_years_25(self, data):
        self.data = data
        data_leaseyears_25 = [x for x in self.data if x[9] == 25]
        return data_leaseyears_25

    # sums the rent of a list

    def sum_rent(self, data):
        self.data = data
        currentRent = [int(float(x[10])) for x in self.data if x[9] == 25]
        return sum(currentRent)

    # returns a dict with tennants and thier mast count as k, v

    def mast_count(self, data):
        self.data = data
        tennant_list = [x[6] for x in self.data]
        tennants = {x: tennant_list.count(x) for x in tennant_list}
        return tennants

    # lease start date between 1st June 1999 and 31st August 2007

    def lease_startdate_between(self, data):
        self.data = data
        start = datetime.strptime("01 Jun 1999", '%d %b %Y').date()
        end = datetime.strptime("31 Aug 2007", '%d %b %Y').date()
        date_range = [x for x in self.data if x[7] >= start and x[7] <= end]
        return date_range

    # custom print method to print list data neatly

    def printl_str(self, data, opt, val):
        self.data = data
        self.opt = opt
        self.val = val
        print(f"Displaying {self.opt[self.val].__name__} results : \n")
        for item in self.data:
            text = (
                f"""{item[0]}, {item[1]}, {item[2]}, {item[3]}, {item[4]},"""
                f"""{item[5]}, {item[6]}, {item[7].strftime('%d/%m/%Y')},"""
                f"""{item[8].strftime('%d/%m/%Y')}, {item[9]}, {item[10]}""")
            print(text)
        print("")
        return

    # custom print method to print dict data neatly

    def printd_str(self, data, opt, val):
        self.data = data
        self.opt = opt
        self.val = val
        print(f"Displaying {self.opt[self.val].__name__} results : \n")
        longest_key = 0
        for k, v in self.data.items():
            if longest_key < len(k):
                longest_key = len(k)
        for k, v in self.data.items():
            print(k.ljust(longest_key, " "), v)
        print("")
        return
