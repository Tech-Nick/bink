import unittest
from MastData import MastData
from datetime import datetime
from unittest.mock import patch
import io
import sys


class TestMastData(unittest.TestCase):

    def setUp(self):
        self.field0 = [
            'Beecroft Hill', 'Broad Lane', '', '', 'LS13',
            'Beecroft Hill - Telecom App', 'Arqiva Services ltd',
            datetime.strptime('01 Mar 1994', '%d %b %Y').date(),
            datetime.strptime('28 Feb 2058', '%d %b %Y').date(),
            64, float(23950)]
        self.field1 = [
            'Potternewton Crescent', 'Potternewton Est Playing Field',
            '', '', 'LS7', 'Potternewton Est Playing Field', 'Arqiva Ltd',
            datetime.strptime('24 Jun 1999',  '%d %b %Y').date(),
            datetime.strptime('23 Jun 2019',  '%d %b %Y').date(),
            20, 6600]
        self.field2 = [
            'Seacroft Gate (Chase) - Block 2', 'Telecomms Apparatus',
            'Leeds', '', 'LS14', 'Seacroft Gate (Chase) block 2-Telecom App.',
            'Vodafone Ltd.',
            datetime.strptime('30 Jan 2004',  '%d %b %Y').date(),
            datetime.strptime('29 Jan 2029',  '%d %b %Y').date(),
            25, 12250]
        self.field3 = [
            'Queenswood Heights', 'Queenswood Heights  Queenswood Gardens',
            'Headingley', '', 'Leeds', 'Queenswood Hgt-Telecom App.',
            'Vodafone Ltd',
            datetime.strptime('08 Nov 2004',  '%d %b %Y').date(),
            datetime.strptime('07 Nov 2029',  '%d %b %Y').date(),
            25, 9500]
        self.field4 = [
            'Armley - Burnsall Grange', 'Armley', 'LS13', '', '',
            'Burnsall Grange CSR 37865', 'O2 (UK) Ltd',
            datetime.strptime('26 Jul 2007',  '%d %b %Y').date(),
            datetime.strptime('25 Jul 2032',  '%d %b %Y').date(),
            25, 12000]
        self.field5 = [
            'Seacroft Gate (Chase) - Block 2', 'Telecomms Apparatus',
            'Leeds', '', 'LS14', 'Seacroft Gate (Chase) - Block 2,  WYK 0414',
            'Hutchinson3G Uk Ltd&Everything Everywhere Ltd',
            datetime.strptime('21 Aug 2007',  '%d %b %Y').date(),
            datetime.strptime('20 Aug 2032', '%d %b %Y').date(), 25, 12750]

        self.data = [
            self.field0, self.field1,
            self.field2, self.field3,
            self.field4, self.field5]

    def tearDown(self):
        pass

    @patch("MastData.MastData")
    def test_MastData(self, mock_MastData):

        self.setUp()

        # given valid data is incoming
        self.new_data = [self.field0]
        mock_MastData = MastData('Python Developer Test Dataset.csv')

        # when MastData('Python Developer Test Dataset.csv') results should be
        self.assertEqual(mock_MastData.csv_data[0], self.new_data[0])

    @patch("MastData.MastData")
    def test_MastData_empty_csv(self, mock_MastData):

        self.setUp()

        # when the MastData('empty.csv') is called the results should be
        self.assertRaises(StopIteration, MastData, 'empty.csv')

    @patch("MastData.MastData")
    def test_MastData_file_not_found(self, mock_MastData):

        self.setUp()

        # when the MastData('') is called the results should be
        self.assertRaises(FileNotFoundError, MastData, '')

    @patch("MastData.MastData")
    def test_MastData_TypeError(self, mock_MastData):

        self.setUp()

        # when the MastData('') is called the results should be
        self.assertRaises(ValueError , MastData, 'wrongtype.csv')

    def test_sort_by_rent(self):

        self.setUp()

        # given valid data is incoming
        self.new_data = [
            self.field1, self.field3,
            self.field4, self.field2,
            self.field5, self.field0]

        # when the sort_by_rent(self, data) is called the results should be
        self.assertEqual(MastData.sort_by_rent(self, self.data), self.new_data)

    def test_first_five(self):

        self.setUp()

        # given valid data is incoming
        self.new_data = [
            self.field1, self.field3,
            self.field4, self.field2,
            self.field5]

        # when the first_five(self, data) is called the results should be
        self.assertEqual(MastData.first_five(self, self.data), self.new_data)

    def test_lease_years_25(self):

        self.setUp()

        # given valid data is incoming
        self.new_data = [self.field2, self.field3, self.field4, self.field5]

        # when the lease_years_25(self, data) is called the results should be
        self.assertEqual(
                MastData.lease_years_25(self, self.data), self.new_data)

    def test_sum_rent(self):
        # given valid data is incoming
        self.setUp()

        self.assertEqual(MastData.sum_rent(self, self.data), 46500)

    def test_mast_count(self):

        self.setUp()

        # given valid data is incoming

        self.new_data_dict = {
            'Arqiva Services ltd': 1, 'Arqiva Ltd': 1,
            'Vodafone Ltd.': 1, 'Vodafone Ltd': 1,
            'O2 (UK) Ltd': 1,
            'Hutchinson3G Uk Ltd&Everything Everywhere Ltd': 1}
        # when the mast_count(self, data) is called the results should be
        self.assertEqual(
                MastData.mast_count(self, self.data), self.new_data_dict)

    def test_lease_startdate_between(self):

        self.setUp()

        # given valid data is incoming
        self.new_data = [
            self.field1, self.field2,
            self.field3, self.field4,
            self.field5]

        # when lease_startdate_between(self, data) is called
        # the results should be
        self.assertEqual(
                MastData.lease_startdate_between(
                    self, self.data), self.new_data)

    def test_printl_str(self):
        self.setUp()

        self.new_data = [self.field0]
        # Create StringIO object
        capturedOutput = io.StringIO()
        #  and redirect stdout.
        sys.stdout = capturedOutput
        self.assertEqual(
            MastData.printl_str(
                    self, self.new_data, {'7': MastData.first_five},
                    '7'), None)
        sys.stdout = sys.__stdout__

    def test_printd_str(self):
        self.setUp()

        self.new_data_dict = {
            'Arqiva Services ltd': 1, 'Arqiva Ltd': 1,
            'Vodafone Ltd.': 1, 'Vodafone Ltd': 1, 'O2 (UK) Ltd': 1,
            'Hutchinson3G Uk Ltd&Everything Everywhere Ltd': 1}

        self.new_data = [self.field0]
        # Create StringIO object
        capturedOutput = io.StringIO()
        #  and redirect stdout.
        sys.stdout = capturedOutput
        self.assertEqual(MastData.printd_str(
            self, self.new_data_dict, {'3': MastData.mast_count}, '3'), None)
        sys.stdout = sys.__stdout__


if __name__ == '__main__':
    unittest.main()
