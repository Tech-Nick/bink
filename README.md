Bink Coding Test Bink.py

2/02/2021 - Nick Costa

This program is to preform the requirements give from the Bink python developer Test

The requirements are as follows

1. Read in the attached file
a. Produce a list sorted by “Current Rent” in ascending order
b. Obtain the first 5 items from the resultant list and output to the console
2. From the list of all mast data, create a new list of mast data with “Lease Years” = 25
years.
a. Output the list to the console, including all data fields
b. Output the total rent for all items in this list to the console
3. Create a dictionary containing tenant name and a count of masts for each tenant
a. Output the dictionary to the console in a readable form
4. List the data for rentals with “Lease Start Date” between 1st June 1999 and 31st
August 2007
a. Output the data to the console with dates formatted as DD/MM/YYYY

The following modules are required
csv
datetime
unittest
patch
io
sys

Installation and Configuration
The program should run as is, providing all the files were downloaded and the file structure was kept

Running
You can run program from command line or ide, the bink.py is the main script and should be run.

Maintainers

Nick Costa

